module gitlab.com/pschlump/pg-con-test

go 1.13

require (
	github.com/lib/pq v1.4.0
	github.com/pschlump/MiscLib v1.0.3
	github.com/pschlump/godebug v1.0.4
	github.com/pschlump/uuid v1.0.3
)
