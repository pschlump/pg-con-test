#!/bin/bash

# go get
# go build
# Assumes ~/.pgpass
#	*:6432:*:*:password
# ./pg-con-test -C "user=postgres dbname=postgres port=6432 host=127.0.0.1 sslmode=disable"
./pg-con-test -C "user=postgres dbname=postgres port=6432 host=127.0.0.1 password=password sslmode=disable"
