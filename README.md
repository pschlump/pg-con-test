
# Connection Test

This connects to PG and runs a query to verify that the connection is live.

```
$ go get
$ go build
./pg-con-test -C "user=postgres dbname=pschlump port=6432 host=127.0.0.1 sslmode=disable"
```

for example.  This example assumes that you have a password in ~/.pgpass for this account.

```
*:5432:*:*:firewood4872208919-u*a8
*:6432:*:*:password
```

Sample Output:

```
Data=[
	{
		"x": 1
	}
]
PASS Success!!! Connected to database

```
